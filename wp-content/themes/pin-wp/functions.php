<?php
// ------------------------------------------------ 
// ---------- Options Framework Theme -------------
// ------------------------------------------------
 require_once ('admin/index.php');

// ----------------------------------------------
// --------------- Load Scripts -----------------
// ----------------------------------------------
 include("functions/scripts.php");

// ---------------------------------------------- 
// --------------- Load Custom Widgets ----------
// ----------------------------------------------
 include("functions/widgets.php");
 include("functions/widgets/widget-tags.php");
 include("functions/widgets/widget-posts.php");
 include("functions/widgets/widget-posts-tags.php");
 include("functions/widgets/widget-top-posts.php");
 include("functions/widgets/widget-cat.php");
 include("functions/widgets/widget-feedburner.php");
 include("functions/widgets/widget-review.php");
 include("functions/widgets/widget-review-rand.php");
 include("functions/widgets/widget-review-recent.php");
 include("functions/widgets/widget-categories.php");
 include("functions/widgets/widget-banner.php");

// ----------------------------------------------
// --------------- Load Custom ------------------
// ---------------------------------------------- 
   include("functions/custom/comments.php");
  
// ----------------------------------------------
// ------ Content width -------------------------
// ----------------------------------------------
if ( ! isset( $content_width ) ) $content_width = 950;

// ----------------------------------------------
// ------ Post thumbnails ----------------------- 
// ----------------------------------------------
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    add_image_size( 'thumbnail-gallery-single', 180, 180, true ); // Gallery thumbnails
    add_image_size( 'thumbnail-blog-featured', 300, 165, true ); // Blog thumbnails home featured posts
    add_image_size( 'thumbnail-blog-masonry', 300, '', true ); // Blog thumbnails home masonry style
    add_image_size( 'thumbnail-widget', 250, 130, true ); // Sidebar Widget thumbnails
    add_image_size( 'thumbnail-widget-small', 55, 55, true ); // Sidebar Widget thumbnails small
	add_image_size( 'thumbnail-single-image', 950, '', true ); // Single thumbnails
}

// ----------------------------------------------
// ------ feed links ----------------------------
// ----------------------------------------------
 add_theme_support( 'automatic-feed-links' );

// ----------------------------------------------
// ------ title tag ----------------------------
// ----------------------------------------------
 add_theme_support( 'title-tag' );
 
// ----------------------------------------------
// ---- Makes Theme available for translation ---
// ----------------------------------------------
 load_theme_textdomain( 'anthemes', get_template_directory() . '/languages' );

// ----------------------------------------------
// -------------- Register Menu -----------------
// ----------------------------------------------
add_theme_support( 'nav-menus' );
add_action( 'init', 'register_my_menus_anthemes' );

function register_my_menus_anthemes() {
    register_nav_menus(
        array(
            'primary-menu' => esc_html__( 'Header Navigation', 'anthemes' ),
            'secondary-menu' => esc_html__( 'Categories Navigation', 'anthemes' ),
        )
    );
}

// ------------------------------------------------ 
// ---- Add  rel attributes to embedded images ----
// ------------------------------------------------ 
function insert_rel_anthemes($content) {
    $pattern = '/<a(.*?)href="(.*?).(bmp|gif|jpeg|jpg|png)"(.*?)>/i';
    $replacement = '<a$1href="$2.$3" class=\'wp-img-bg-off\' rel=\'mygallery\'$4>';
    $content = preg_replace( $pattern, $replacement, $content );
    return $content;
}
add_filter( 'the_content', 'insert_rel_anthemes' );

// ---- Add  rel attributes to gallery images ----
add_filter('wp_get_attachment_link', 'add_gallery_id_rel_anthemes');
function add_gallery_id_rel_anthemes($link) {
    global $post;
    return str_replace('<a href', '<a rel="mygallery" class="wp-img-bg-off" href', $link);
}


// ------------------------------------------------ 
// --- Pagination class/style for entry articles --
// ------------------------------------------------ 
function custom_nextpage_links_anthemes($defaults) {
$args = array(
'before' => '<div class="my-paginated-posts"><p>' . '<span>',
'after' => '</span></p></div>',
);
$r = wp_parse_args($args, $defaults);
return $r;
}
add_filter('wp_link_pages_args','custom_nextpage_links_anthemes');


// ------------------------------------------------ 
// ------------ Nr of Topics for Tags -------------
// ------------------------------------------------  
add_filter ( 'wp_tag_cloud', 'tag_cloud_count_anthemes' );
function tag_cloud_count_anthemes( $return ) {
return preg_replace('#(<a[^>]+\')(\d+)( topics?\'[^>]*>)([^<]*)<#imsU','$1$2$3$4 <span>($2)</span><',$return);
}


// ------------------------------------------------ 
// --------------- Posts Time Ago -----------------
// ------------------------------------------------  

function time_ago_anthemes( $type = 'post' ) {
    $d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
    return human_time_diff($d('U'), current_time('timestamp')) . " ";
}


// ------------------------------------------------ 
// --------------- Author Social Links ------------
// ------------------------------------------------ 
function anthemes_contactmethods( $contactmethods ) {
    $contactmethods['twitter']   = ''. esc_html__('Twitter Username', 'anthemes') .'';
    $contactmethods['facebook']  = ''. esc_html__('Facebook Username', 'anthemes') .'';
    $contactmethods['google']    = ''. esc_html__('Google+ Username', 'anthemes') .'';
    return $contactmethods;
}
add_filter('user_contactmethods','anthemes_contactmethods',10,1);


// ----------------------------------------------
// ---------- excerpt length adjust -------------
// ----------------------------------------------
function anthemes_excerpt($str, $length, $minword = 3)
{
    $sub = '';
    $len = 0;
    
    foreach (explode(' ', $str) as $word)
    {
        $part = (($sub != '') ? ' ' : '') . $word;
        $sub .= $part;
        $len += strlen($part);
        
        if (strlen($word) > $minword && strlen($sub) >= $length)
        {
            break;
        }
    }
    
    return $sub . (($len < strlen($str)) ? ' ..' : '');
}
 


// ------------------------------------------------ 
// ------------ Number of post views --------------
// ------------------------------------------------

 // function to display number of posts.
function getPostViews_anthemes($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return '0 <span>' . esc_html__('View', 'anthemes') . '</span>';
    }
    return $count.' <span>' . esc_html__('Views', 'anthemes') . '</span>';
}

// function to count views.
function setPostViews_anthemes($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


// ------------------------------------------------ 
// ------------ Meta Box --------------------------
// ------------------------------------------------
$prefix = 'anthemes_';
global $meta_boxes;
$meta_boxes = array();

// 1st meta box
$meta_boxes[] = array(
    'id' => 'standard',
    'title' => esc_html__( 'Article Page Options', 'anthemes' ),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,

    // Youtube
    'fields' => array(

[
            'name'  => esc_html__( 'UID', 'anthemes' ),
            'id'    => "_uid",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
        
        ,[
            'name'  => esc_html__( 'Brand', 'anthemes' ),
            'id'    => "_brand",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
        ,[
            'name'  => esc_html__( 'Source', 'anthemes' ),
            'id'    => "_source",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
        ,[
            'name'  => esc_html__( 'Price', 'anthemes' ),
            'id'    => "_price",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
        ,
        [
            'name'  => esc_html__( 'Sale Price', 'anthemes' ),
            'id'    => "_salePrice",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
         ,
        [
            'name'  => esc_html__( 'Image', 'anthemes' ),
            'id'    => "_image",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ]
        ,
        [
            'name'  => esc_html__( 'Buy Url', 'anthemes' ),
            'id'    => "_buyUrl",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ],
        [
            'name'  => esc_html__( 'Tags', 'anthemes' ),
            'id'    => "_tag",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ],
        [
            'name'  => esc_html__( 'Color', 'anthemes' ),
            'id'    => "_color",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ],
        [
            'name'  => esc_html__( 'Shipping', 'anthemes' ),
            'id'    => "_shipping",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ],
        [
            'name'  => esc_html__( 'Market Country', 'anthemes' ),
            'id'    => "_marketCountry",
            'type'  => 'text',
            'std'   => esc_html__( '', 'anthemes' ),
            'clone' => false,
        ],

        // TEXT
        array(
            // Field name - Will be used as label
            'name'  => esc_html__( 'Video Youtube', 'anthemes' ),
            // Field ID, i.e. the meta key
            'id'    => "{$prefix}youtube",
            // Field description (optional)
            'desc'  => esc_html__( 'Add Youtube code ex: HIrMIeN5ttE', 'anthemes' ),
            'type'  => 'text',
            // Default value (optional)
            'std'   => esc_html__( '', 'anthemes' ),
            // CLONES: Add to make the field cloneable (i.e. have multiple value)
            'clone' => false,
        ),


    // Vimeo
        // TEXT
        array(
            // Field name - Will be used as label
            'name'  => esc_html__( 'Video Vimeo', 'anthemes' ),
            // Field ID, i.e. the meta key
            'id'    => "{$prefix}vimeo",
            // Field description (optional)
            'desc'  => esc_html__( 'Add Vimeo code ex: 7449107', 'anthemes' ),
            'type'  => 'text',
            // Default value (optional)
            'std'   => esc_html__( '', 'anthemes' ),
            // CLONES: Add to make the field cloneable (i.e. have multiple value)
            'clone' => false,
        ),

    // Gallery
        // IMAGE UPLOAD
        array(
            'name' => esc_html__( 'Gallery', 'anthemes' ),
            'id'   => "{$prefix}slider",
            // Field description (optional)
            'desc'  => esc_html__( 'Image with any size!', 'anthemes' ),            
            'type' => 'image_advanced',
        ),

    // Hide Featured Image
        // CheckBox
        array(
            'name' => esc_html__( 'Featured Image', 'anthemes' ),
            'id'   => "{$prefix}hideimg",
            'desc'  => esc_html__( 'Hide Featured Image on single page for this article', 'anthemes' ),
            'type' => 'checkbox',
        ),


    ),

);



/**
 * Register meta boxes
 *
 * @return void
 */
function anthemes_register_meta_boxes()
{
    // Make sure there's no errors when the plugin is deactivated or during upgrade
    if ( !class_exists( 'RW_Meta_Box' ) )
        return;

    global $meta_boxes;
    foreach ( $meta_boxes as $meta_box )
    {
        new RW_Meta_Box( $meta_box );
    }
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'anthemes_register_meta_boxes' );


// ------------------------------------------------ 
// ---------- TGM_Plugin_Activation -------------
// ------------------------------------------------ 
require_once dirname( __FILE__ ) . '/functions/custom/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

function my_theme_register_required_plugins() {

    $plugins = array(
        array(
            'name'                  => 'Meta Box', // The plugin name
            'slug'                  => 'meta-box', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/plugins/meta-box.zip', // The plugin source
            'required'              => true, // If false, the plugin is only 'recommended' instead of required
            'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'Shortcodes', // The plugin name
            'slug'                  => 'anthemes-shortcodes', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/plugins/anthemes-shortcodes.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '1.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'Reviews', // The plugin name
            'slug'                  => 'anthemes-reviews', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/plugins/anthemes-reviews.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'Thumbs Likes System', // The plugin name
            'slug'                  => 'thumbs-rating', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/plugins/thumbs-rating.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'AccessPress Anonymous Post',
            'slug'                  => 'accesspress-anonymous-post',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'Custom Sidebars',
            'slug'                  => 'custom-sidebars',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'Resume Builder',
            'slug'                  => 'resume-builder',
            'required'              => false,
            'version'               => '',
        ),        

        array(
            'name'                  => 'Daves WordPress Live Search',
            'slug'                  => 'daves-wordpress-live-search',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'Multi-column Tag Map',
            'slug'                  => 'multi-column-tag-map',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'Responsive Menu',
            'slug'                  => 'responsive-menu',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'Social Count Plus',
            'slug'                  => 'social-count-plus',
            'required'              => false,
            'version'               => '',
        ),

        array(
            'name'                  => 'WP-PageNavi',
            'slug'                  => 'wp-pagenavi',
            'required'              => false,
            'version'               => '',
        ),

    );

    // Change this to your theme text domain, used for internationalising strings
    $theme_text_domain = 'tgmpa';
    $config = array(
        'domain'            => $theme_text_domain,          // Text domain - likely want to be the same as your theme.
        'default_path'      => '',                          // Default absolute path to pre-packaged plugins
        'parent_menu_slug'  => 'themes.php',                // Default parent menu slug
        'parent_url_slug'   => 'themes.php',                // Default parent URL slug
        'menu'              => 'install-required-plugins',  // Menu slug
        'has_notices'       => true,                        // Show admin notices or not
        'is_automatic'      => false,                       // Automatically activate plugins after installation or not
        'message'           => '',                          // Message to output right before the plugins table
        'strings'           => array(
            'page_title'                                => esc_html__( 'Install Required Plugins', 'video_wp' ),
            'menu_title'                                => esc_html__( 'Install Plugins', 'video_wp' ),
            'installing'                                => esc_html__( 'Installing Plugin: %s', 'video_wp' ), // %1$s = plugin name
            'oops'                                      => esc_html__( 'Something went wrong with the plugin API.', 'video_wp' ),
            'return'                                    => esc_html__( 'Return to Required Plugins Installer', 'video_wp' ),
            'plugin_activated'                          => esc_html__( 'Plugin activated successfully.', 'video_wp' ),
            'complete'                                  => esc_html__( 'All plugins installed and activated successfully. %s', 'video_wp' ), // %1$s = dashboard link
            'nag_type'                                  => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );

    tgmpa( $plugins, $config );

}

?>
<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <?php
    // Options from admin panel
    global $smof_data;

    $favicon = $smof_data['custom_favicon'];
    if (empty($favicon)) {
        $favicon = get_template_directory_uri() . '/images/web-icon.png';
    }

    $site_logo = $smof_data['site_logo'];
    if (empty($site_logo)) {
        $site_logo = get_template_directory_uri() . '/images/logo.png';
    }

    if (empty($smof_data['featured-posts'])) {
        $smof_data['featured-posts'] = '12';
    }
    $boxed_version_select = (isset($smof_data['boxed_version_select'])) ? $smof_data['boxed_version_select'] : 'No';
    ?>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>

    <!-- Title -->
    <?php if (!function_exists('_wp_render_title_tag')) {
        function theme_slug_render_title()
        { ?>
            <title><?php wp_title('|', true, 'right'); ?></title>
        <?php }

        add_action('wp_head', 'theme_slug_render_title');
    } // Backwards compatibility for older versions. ?>

    <!-- Mobile Device Meta -->
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui'/>

    <!-- The HTML5 Shim for older browsers (mostly older versions of IE). -->
    <!--[if IE]>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script> <![endif]-->

    <!-- Favicons and rss / pingback -->
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed"
          href="<?php esc_url(bloginfo('rss2_url')); ?>"/>
    <link rel="pingback" href="<?php esc_url(bloginfo('pingback_url')); ?>"/>
    <link rel="shortcut icon" type="image/png" href="<?php echo esc_url($favicon); ?>"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <!-- this maybe should be changed for @fontface-->

    <!-- Custom style -->
    <?php echo get_template_part('custom-style'); ?>

    <!-- Theme output -->
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo includes_url('css/jquery-ui.css') ?>">

</head>
<body <?php if ($boxed_version_select == 'Yes') { ?>id="boxed-style" <?php body_class(); ?>>
<?php if (!empty($smof_data['background_img'])) { ?>
    <img id="background" src="<?php echo esc_url($smof_data['background_img']); ?>" alt="background img"/>
<?php }
} else { ?><?php body_class(); ?>> <?php } // background image ?>

<!-- Begin Header -->
<header>
    <div class="main-header">
        <div class="sticky-on">

            <div class="header-inner cf">

                <a href="<?php echo esc_url(home_url('/')); ?>">
                    <h2 class="site-title">stylecross</h2>
                </a>
                <?php get_search_form(); ?>
            </div>
            <div class="bar-header">
                
                <!--<div class="header-banner">
                            <p>Up To 70% OFF Designer Collection - Fresh Spring & End Of Winter Sales Are In! <a href="http://joyofsale.com/shopbop70" target="_blank" rel="nofollow"><span>SHOP NOW</span></a></p>   
                        </div>-->
                
                <div class="wrap-center">
                    <!-- Navigation Menu Categories -->
                    <div class="menu-categories">
                        <nav id="myjquerymenu-cat" class="jquerycssmenu">
                            <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul>%3$s<i class="fa fa-heart"></i></ul>', 'theme_location' => 'secondary-menu')); ?>

                        </nav>
                    </div><!-- end .menu-categories -->
                </div>
            </div>

        </div>

        <div class="clear"></div>
    </div><!-- end .main-header -->
    
    <!--<div class="header-banner under-1000">
                            <p>Up To 70% OFF Designer Collection - Fresh Spring & End Of Winter Sales Are In! <a href="http://joyofsale.com/shopbop70" target="_blank" rel="nofollow"><span>SHOP NOW</span></a></p>   
                        </div>-->

</header><!-- end #header -->


<?php if (term_exists('featured', 'post_tag')) { ?>
    <?php if (is_front_page()) { ?>
        <!-- Begin Featured articles on slide -->
        <div class="featured-articles">

            <div class="featured-title">
                <h3><?php esc_html_e('Featured Articles', 'anthemes'); ?></h3>
                <div class="slide-nav">
                    <span id="slider-prev"></span>
                    <span id="slider-next"></span>
                </div><!-- end .slide-nav -->
            </div><!-- end .featured-title -->

            <ul class="featured-articles-slider">
                <?php query_posts(array('post_type' => 'post', 'tag' => 'featured', 'posts_per_page' => esc_attr($smof_data['featured-posts']))); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <li <?php post_class('post-slide') ?>>
                        <?php if (has_post_thumbnail()) { ?>

                            <div
                                class="article-comm"><?php comments_popup_link('<i class="fa fa-comments"></i> 0', '<i class="fa fa-comments"></i> 1', '<i class="fa fa-comments"></i> %'); ?></div>
                            <div class="article-category"><i></i> <?php $category = get_the_category();
                                if ($category) {
                                    echo wp_kses_post('<a href="' . get_category_link($category[0]->term_id) . '" class="tiptipBlog" title="' . sprintf(esc_html__("View all posts in %s", "anthemes"), $category[0]->name) . '" ' . '>' . $category[0]->name . '</a> ');
                                } ?>
                            </div><!-- end .article-category -->

                            <?php the_post_thumbnail('thumbnail-blog-featured', array('title' => "")); ?>
                        <?php } else { ?>
                            <?php $postMeta = get_post_meta(get_the_ID()); ?>

                            <img class="attachment-thumbnail-blog-featured wp-post-image "
                                 onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';"
                                 src="<?php echo rwmb_meta('_image') ?>"
                            />
                        <?php } // Post Thumbnail ?>

                        <div class="title-box">
                            <?php if (rwmb_meta('_salePrice')) { ?>
                                <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                            <?php } else { ?>
                                <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                            <?php } ?>

                            <?php if (function_exists('taqyeem_get_score')) { ?><?php taqyeem_get_score(); ?><?php } ?>
                            <div class="clear"></div>
                            <h2><a href="<?php the_permalink(); ?>"><?php if (strlen(get_the_title()) > 70) {
                                        echo substr(get_the_title(), 0, 66) . " ...";
                                    } else {
                                        the_title('');
                                    } ?></a></h2>
                        </div>

                    </li><!-- end .post-slide -->
                <?php endwhile; endif;
                wp_reset_query(); ?>
            </ul><!-- end .featured-articles-slider -->

            <div class="clear"></div>
        </div><!-- end .featured-articles -->
    <?php }
} ?>

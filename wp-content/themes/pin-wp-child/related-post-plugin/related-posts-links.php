<?php
/**
 * Widget and shortcode template: links template.
 *
 * This template is used by the plugin: Related Posts by Taxonomy.
 *
 * plugin:        https://wordpress.org/plugins/related-posts-by-taxonomy
 * Documentation: https://keesiemeijer.wordpress.com/related-posts-by-taxonomy/
 *
 * @package Related Posts by Taxonomy
 * @since 0.1
 *
 * The following variables are available:
 *
 * @var array $related_posts Array with related posts objects or empty array.
 * @var array $rpbt_args     Widget or shortcode arguments.
 */
?>

<?php
/**
 * Note: global $post; is used before this template by the widget and the shortcode.
 */
?>

<?php if ( $related_posts ) : ?>
<div class="underline"></div>
<div class="arrow-down-related"></div>
<ul class="article_list">
	<?php foreach ( $related_posts as $post ) :
		setup_postdata( $post ); ?>
		<li>
            
            <h4 class="article-title">
                <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>">
                    <div class="title-overflow"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></div>
                    <img src="<?php echo rwmb_meta('_image')?>" />
                    <div class="round-search"><i class="fa fa-eye"></i></div>
                    
                    <p class="price">
                    <?php if (rwmb_meta('_salePrice')) { ?>
                        <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                        <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                    <?php } else { ?>
                        <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                    <?php } ?>
                  </p>
                    
                </a><div class="img-border"></div>
            </h4>
    </li>
	<?php endforeach; ?>
</ul>

<?php else : ?>
<p><?php _e( 'No related posts found', 'related-posts-by-taxonomy' ); ?></p>
<?php endif ?>

<?php
/**
 * note: wp_reset_postdata(); is used after this template by the widget and the shortcode.
 */
?>